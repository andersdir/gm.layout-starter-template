'use strict'

var gulp = require('gulp'),
    pug = require('gulp-pug'),
    autoprefixer = require('gulp-autoprefixer'),
    del = require('del'),
    fs = require('fs'),
    browserSync = require('browser-sync').create();

// Генерируем версту с помощью шаблонизатора PUG
gulp.task('pug', function () {
    return gulp.src('src/pug/pages/*.pug')
        .pipe(pug({
            locals : {
                data: JSON.parse(fs.readFileSync('src/data/data.json', 'utf8')),
            },
            pretty : true
        }))
        .pipe(gulp.dest('build'))
        .on('end', browserSync.reload);
});

// Переносим все содержимое папки plugins
gulp.task('plugins', function () {
    return gulp.src('src/assets/plugins/**/*')
        .pipe(gulp.dest('build/assets/plugins'))
        .on('end', browserSync.reload);
});

// Переносим все содержимое папки images
gulp.task('images', function () {
    return gulp.src('src/assets/images/**/*')
        .pipe(gulp.dest('build/assets/images'))
        .on('end', browserSync.reload);
});

// Переносим все содержимое папки files
gulp.task('files', function () {
    return gulp.src('src/assets/files/**/*')
        .pipe(gulp.dest('build/assets/files'))
        .on('end', browserSync.reload);
});

// Переносим все css из папки css
gulp.task('css', function () {
    return gulp.src('src/assets/css/*.css')
        .pipe(autoprefixer({
            cascade: true
        }))
        .pipe(gulp.dest('build/assets/css'))
        .pipe(browserSync.stream());
});


// Переносим все js из папки js
gulp.task('js', function () {
    return gulp.src('src/assets/js/*.js')
        .pipe(gulp.dest('build/assets/js'))
        .on('end', browserSync.reload);
});

// Запускаем процесс обновленяи страниц если нужно
gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
});

// Cледим за изменениями файлов и запускаем нужные task если были зафикисрвоаны изменения
gulp.task('watch', function () {
    gulp.watch(['src/pug/**/*.pug','src/data/**/*.json'], gulp.series('pug'));
    gulp.watch('src/assets/plugins/**/*.*', gulp.series('plugins'));
    gulp.watch('src/assets/images/**/*.*', gulp.series('images'));
    gulp.watch('src/assets/files/**/*.*', gulp.series('files'));
    gulp.watch('src/assets/css/**/*.css', gulp.series('css'));
    gulp.watch('src/assets/js/**/*.js', gulp.series('js'));
});


// Удаляем содержимое билда
gulp.task('clean', function() {
    return del([
        './build'
    ]);
});

// ОСНОВНАЯ ЗАДАЧА. Запускается по команде GULP. Сначала проходится по списку задач, потому запускает отслеживание изменений и запускает браузер
gulp.task('default', gulp.series(
    gulp.parallel('pug','plugins','images','files','css', 'js'),
    gulp.parallel('watch','serve')
));

//
gulp.task('build', gulp.series(
    gulp.parallel('pug','plugins','images','files','css', 'js')
));
