# Стартовый шаблон верстки многостраничного сайта

Был разработан шаблон, по которому ведется вся верстка многостраничных сайтов. Его нужно скачать, установить и работать по установленному флоу.

Шаблон построен на NPM и GULP. Использует html-шаблонизатор PUG. Это позволяет автоматизировать многие задачи, а так же сократить время на верстку.

Чтобы понимать, как пользоваться шаблоном, нужно знать html-шаблонизатор PUG и основы GULP. Следует просмотреть обучающие курсы или изучить документацию. Все это достаточно просто.

**Подключенные библиотеки:**

- jQuery
- Slick Slider - плагин для создания слайдеров
- Bootstrap Grid - от бутрстапа подключена только сетка, чтобы было удобно верстать
- Fancybox - простая работа с попапами

## Разворачивание шаблона

Предварительно должен быть установлен node.js и git.

1. Скачиваем отсюда шаблон: [https://gitlab.com/andersdir/gm.layout-starter-template](https://gitlab.com/andersdir/gm.layout-starter-template)
2. Заходим в паку через терминал и устанавливаем пакеты: `npm install`
3. После установки в терминале запустите первый билд: `gulp build`

Все, шаблон готов к работе.

## Стандарты работы

### Правила:

- Вся работа над проектом ведется в гит.
- Разработка ведется в папке src, затем через `gulp build` билдится проект в папку build.
- В папке build мы не вносим никаких изменений вручную. Только через src и создание билда.
- На продакшн или на дев мы отправляем **только папку build**. Переносим вручную.

### Стандартный флоу

1. Скачиваем и разворачиваем шаблон
2. Заносим проект в гит
3. Создаем ветку из мастера, вносим изменения в src.
4. Билдим через терминал `gulp build`
5. Мерджим в мастер, билдим
6. Проверяем. Если все ок - отправляем пушим в репозиторий
7. Если нужно передать/показать клиенту - переносим содержимое build куда требуется

## Команды

`gulp build` - запускает сборку из src в папку build

`gulp clean` - очищает папку с билдом. Иногда требуется, если файлы какие-то удалили, например. Потому что простой билд только изменяет/добавляет файлы. Но не удаляет, если вы из src что-то удалили.

`gulp` - главная рабочая команда. Она создает/обновляет билд, открывает сборку в браузере и обновляется, если внесены изменения. Сборка тоже автоматически обновляется. Это фоновый процесс. Если нужно выйти - нужно нажать `ctrl + c` и подтвердить выбор `y`

## Структура и описание папки src

- **assets** - в этой папке содержатся все ассеты (стили, скрипты, плагины, изображения, видео и т.д.). При билде все содержимое папки assets дублируется в билд без измененийю
    - css
    - files
    - images
    - js
    - plugins
- data
    - data.json - это файл с различным контентом, который может понадобится в шаблоне. Например можно внести туда список статей, а потом по шаблону их выводить. Пример есть в pug/modules/header.pug. Если нужно добавить отдельный файл с другими данными, то нужно будет добавить его подключение в gulpfile.js в task pug аналогично текущему файлу.
- pug - это шаблоны наших страниц и блоков. См. html-шаблонизатор PUG. Содержимое компилируется в страницы и размещает их в папке build.
    - layout - основные шаблоны
    - modules - блоки (например header.pug)
    - pages - сами страницы
    - mixins.pug - файл для библиотеки миксинов